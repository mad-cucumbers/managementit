﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.ResponseModels
{
    //Постепенно будем расширять это класс, в дальнейшем хочу добавить больше информативности о полученных ошибках
    public class NotificationViewModel
    {
        //Использовать для TempData
        public const string TempDataKey = "NotificationData";

        //Название совершаемого действия
        public string Title { get; set; }

        //Описание совершаемого действия
        public string Text { get; set; }

        //Результат в формате строки
        public string Typestr { get { return Type.ToString().ToLower(); } }

        //Результат действия
        public NotificationType Type { get; set; }

        //Тип ошибки
        public TypeOfErrors Error { get; set; }
        //Ошибка AspNetCore
        public Exception ExceptionAspNet { get; set; }


        public NotificationViewModel(string title, string text, NotificationType type)
        {
            Title = title;
            Text = text;
            Type = type;
        }

        public NotificationViewModel(string title, TypeOfErrors error, Exception ex)
        {
            Title = title;
            Text = ErrorHelper(error);
            Type = NotificationType.Error;
            Error = error;
            ExceptionAspNet = ex;
        }

        protected static string ErrorHelper(TypeOfErrors error)
        {
            var result = "";

            switch (error)
            {
                case TypeOfErrors.AccessDenied:
                    result = $"Недостаточно прав!";
                    break;
                case TypeOfErrors.NotFound:
                    result = $"Данные не найдены!";
                    break;
                case TypeOfErrors.DataNotValid:
                    result = $"Данные не действительны!";
                    break;
                case TypeOfErrors.InternalError:
                    result = $"Произошла внутренняя ошибка!";
                    break;

                default:
                    result = $"Неизвестная ошибка!";
                    break;
            }

            return result;
        }
    }

    public enum NotificationType
    {
        Success = 1,
        Info,
        Warn,
        Error
    }

    public enum TypeOfErrors
    {
        AccessDenied = 1,
        NotFound,
        InternalError,
        DataNotValid,
    }
}
