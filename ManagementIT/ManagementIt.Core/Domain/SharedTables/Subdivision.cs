﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.SharedTables
{
    public class Subdivision : BaseEntity
    {
        [Display(Name = "Название подразделения")]
        public override string Name { get; set; }
    }
}
