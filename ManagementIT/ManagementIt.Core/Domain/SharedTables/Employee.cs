﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.SharedTables
{
    public class Employee : BaseEntity
    {
        [Display(Name = "Фамилия сотрудника")]
        public string Surname { get; set; }
        
        [Display(Name = "Имя сотрудника")]
        public override string Name { get; set; }
        
        [Display(Name = "Отчество сотрудника")]
        public string Patronymic { get; set; }
        
        [Display(Name = "отдел")]
        public Departament Departament { get; set; }
        
        [Display(Name = "Должность")]
        public Position Position { get; set; }
        
        [Display(Name = "рабочий телефон")]
        public string WorkTelephone { get; set; }
        
        [Display(Name = "Мобальный телефон")]
        public string MobileTelephone { get; set; }
        
        [Display(Name = "Рабочая почта")]
        public string Mail { get; set; }
        
        [Display(Name = "Связанный пользователь в АД")]
        public string User { get; set; }
        
        [Display(Name = "Фото пользователя")]
        public string Photo { get; set; }
        
        [Display(Name = "Роли")]
        public List<Role> Roles { get; set; }

        public string GetSNPCut()
        {
            return string.IsNullOrEmpty(Name) ? "" : $"{Surname} {Name[0]}. {Patronymic[0]}.";
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(GetSNPCut()) ? "" : GetSNPCut();
        }

    }
}
