﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.SharedTables
{
    public class Building : BaseEntity
    {
        [Required]
        [Display(Name = "Название корпуса")]
        public override string Name { get; set; }
        
        [Required]
        [Display(Name = "Адрес корпуса")]
        public string Address { get; set; }
        
        [Display(Name = "Кабинеты")]
        public List<Room> Rooms { get; set; }
        
        [Required]
        [Display(Name = "Количество этажей")]
        public int Floor { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
