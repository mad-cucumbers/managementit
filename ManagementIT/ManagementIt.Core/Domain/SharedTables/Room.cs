﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.SharedTables
{
    public class Room : BaseEntity
    {
        [Display(Name = "Здание")]
        public Building Building { get; set; }
        
        [Required]
        [Display(Name = "На каком этаже")]
        public int Floor { get; set; }
        
        [Required]
        [Display(Name = "Отделение")]
        public Departament Departament { get; set; }
        
        [Display(Name = "Требуется розеток")]
        public int RequiredCountSocket { get; set; }
        
        [Display(Name = "Текущие кол-во розеток")]
        public int CurrentCountSocket { get; set; }

        public override string ToString()
        {
            return base.Name;
        }

        public string GetFullNameRoom()
        {
            return $"{Building.Name}=>{base.Name}";
        }

    }
}
