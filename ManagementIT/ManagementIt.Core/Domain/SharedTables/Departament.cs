﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.SharedTables
{
    public class Departament : BaseEntity
    {
        [Display(Name = "Название отдела")]
        public override string Name { get; set; }
        
        [Display(Name = "Руководитель")]
        public string Supervisor { get; set; }
        
        [Display(Name = "Заведующий")]
        public string Headof { get; set; }
        
        [Display(Name = "Подразделение")]
        public Subdivision Subdivision { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
