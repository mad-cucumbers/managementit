﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.SharedTables
{
    public class Position : BaseEntity
    {
        [Display(Name = "Должность")]
        public override string Name { get; set; }
    }
}
