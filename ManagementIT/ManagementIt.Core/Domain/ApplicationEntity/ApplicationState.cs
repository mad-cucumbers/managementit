﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.ApplicationEntity
{
    //Состояние заявки
    public class ApplicationState : BaseEntity
    {
        [Display(Name = "Состояние")]
        public string State { get; set; }
        [Display(Name = "Цвет")]
        public string BGColor { get; set; }

        public override string ToString()
        {
            return State;
        }
    }
}
