﻿using ManagementIt.Core.Domain.SharedTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.ApplicationEntity
{
    public class ApplicationToIt : BaseEntity
    {
        [Display(Name = "Приоритет")]
        public ApplicationPriority Priority { get; set; }

        [Display(Name = "Тип")]
        public ApplicationType Type { get; set; }
        
        [Display(Name = "Текст заявки")]
        public string Content { get; set; }
        
        [Display(Name = "Примечание")]
        public string Note { get; set; }
        
        [Display(Name = "Отделение")]
        public Departament Departament { get; set; }
       
        [Display(Name = "Помещение")]
        public Room Room { get; set; }
        
        [Display(Name = "ФИО подавшего")]
        public Employee Employee { get; set; }
       
        [Display(Name = "Контактные данные")]
        public string Contact { get; set; }

    }
}
