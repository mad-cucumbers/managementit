﻿using ManagementIt.Core.Domain.SharedTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.ApplicationEntity
{
    //Событие связанное с заявкой, история заявки
    public class ApplicationAction : BaseEntity
    {
        [Required]
        [Display(Name = "Номер заявки")]
        public ApplicationToIt ApplicationToIt { get; set; }

        [Display(Name = "Состояние")]
        public ApplicationState State { get; set; }
        
        [Display(Name = "Дата и время собития")]
        public DateTime EventDateAndTime { get; set; }
        
        [Display(Name = "Инициатор события")]
        public Employee InitiatorEmployee { get; set; }

    }
}
