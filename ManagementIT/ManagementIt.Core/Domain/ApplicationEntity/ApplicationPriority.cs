﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Domain.ApplicationEntity
{
    //Приоритет заявки
    public class ApplicationPriority : BaseEntity
    {
        public override string ToString()
        {
            return base.Name;
        }
    }
}
