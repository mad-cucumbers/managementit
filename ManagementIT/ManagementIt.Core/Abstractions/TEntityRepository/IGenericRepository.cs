﻿using ManagementIt.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Abstractions.TEntityRepository
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllEntitiesAsync();
        Task<IEnumerable<T>> GetEntityByNameAsync(string name);
        Task<T> GetEntityByIdAsync(int id);
        Task<int> AddEntityAsync(T entity);
        Task<T> UpdateEntityAsync(T entity);
        Task DeleteEntityAsync(T entity);

        IEnumerable<T> GetAllEntities();
        IEnumerable<T> GetEntityByName(string name);
        T GetEntityById(int id);
        int AddEntity(T entity);
        T UpdateEntity(T entity);
        void DeleteEntity(T entity);
    }
}
