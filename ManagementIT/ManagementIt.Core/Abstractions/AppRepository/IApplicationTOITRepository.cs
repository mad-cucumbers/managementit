﻿using ManagementIt.Core.Abstractions.TEntityRepository;
using ManagementIt.Core.Domain.ApplicationEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.Core.Abstractions.AppRepository
{
    public interface IApplicationTOITRepository : IGenericRepository<ApplicationToIt>
    {
        Task<List<ApplicationToIt>> GetAppByDepartamentIdAsync(int depId);
        Task<List<ApplicationToIt>> GetAppByApplicationTypeIdAsync(int typeId);

        List<ApplicationToIt> GetAppByDepartamentId(int depId);
        List<ApplicationToIt> GetAppByApplicationTypeId(int typeId);
    }
}
