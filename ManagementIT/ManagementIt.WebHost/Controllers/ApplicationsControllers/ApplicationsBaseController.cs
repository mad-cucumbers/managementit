﻿using ManagementIt.Core.Abstractions.AppRepository;
using ManagementIt.Core.Domain.ApplicationEntity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementIt.WebHost.Controllers.ApplicationsControllers
{
    public abstract class ApplicationsBaseController : ControllerBase
    {
        protected readonly IApplicationTOITRepository _applicationTOITRepository;

        public ApplicationsBaseController(IApplicationTOITRepository applicationTOITRepository)
        {
            _applicationTOITRepository = applicationTOITRepository;
        }

        [HttpGet("{departamentId}")]
        public async Task<ActionResult<ApplicationToIt>> GetApplicationsByDepartament(int departamentId)
        {
            var applications = await _applicationTOITRepository.GetAppByDepartamentIdAsync(departamentId);
            return Ok(applications);
        }

        [HttpGet]
        public async Task<ActionResult<ApplicationToIt>> GetApplicationsAll()
        {
            var applications = await _applicationTOITRepository.GetAllEntitiesAsync();
            return Ok(applications);
        }

        [HttpPut]
        public async Task<ActionResult<ApplicationToIt>> UpdateApplicationAsync(ApplicationToIt applicationToIt)
        {
            var applications = await _applicationTOITRepository.UpdateEntityAsync(applicationToIt);
            return Ok(applications);
        }

        [HttpPost]
        public async Task<ActionResult<ApplicationToIt>> CreateApplicationAsync(ApplicationToIt applicationToIt)
        {
            var response = await _applicationTOITRepository.AddEntityAsync(applicationToIt);
            return Ok(response);
        }

    }
}
