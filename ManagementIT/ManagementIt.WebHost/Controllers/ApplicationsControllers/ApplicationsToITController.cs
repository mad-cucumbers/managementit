﻿using ManagementIt.Core.Abstractions.AppRepository;
using ManagementIt.Core.Domain.ApplicationEntity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementIt.WebHost.Controllers.ApplicationsControllers
{
    [ApiController]
    [Route("Applications")]
    public class ApplicationsToITController : ApplicationsBaseController
    {
        public ApplicationsToITController(IApplicationTOITRepository applicationTOITRepository): base(applicationTOITRepository)
        {
        }
    }
}
