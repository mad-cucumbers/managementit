﻿using ManagementIt.Core.Domain.ApplicationEntity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.DataAccess.DataBase
{
    public class AppDbContext : DbContext
    {
        public DbSet<ApplicationAction> ApplicationsAction { get; set; }
        public DbSet<ApplicationToIt> ApplicationsToIt { get; set; }
        public DbSet<ApplicationType> ApplicationsType { get; set; }
        public DbSet<ApplicationState> ApplicationsState { get; set; }
        public DbSet<ApplicationPriority> ApplicationsPriority { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
    }
}
