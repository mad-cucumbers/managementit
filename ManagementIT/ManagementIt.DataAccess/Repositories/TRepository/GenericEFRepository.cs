﻿using ManagementIt.Core.Abstractions.TEntityRepository;
using ManagementIt.Core.Domain;
using ManagementIt.DataAccess.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.DataAccess.Repositories.TRepository
{
    public class GenericEFRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _context;

        public GenericEFRepository(AppDbContext context)
        {
            _context = context;
        }

        public int AddEntity(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        public Task<int> AddEntityAsync(T entity)
        {
            _context.Set<T>().AddAsync(entity);
            _context.SaveChangesAsync();
            return Task.FromResult(entity.Id);
        }

        public void DeleteEntity(T entity)
        {
            _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            _context.SaveChanges();
        }

        public async Task DeleteEntityAsync(T entity)
        {
            _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public IEnumerable<T> GetAllEntities()
        {
            var response = _context.Set<T>().AsEnumerable();
            return response;
        }

        public async Task<IEnumerable<T>> GetAllEntitiesAsync()
        {
            var response = _context.Set<T>().AsEnumerable();
            return await Task .FromResult(response);
        }

        public T GetEntityById(int id)
        {
            var response = _context.Set<T>().Find(id);
            return response;
        }

        public async Task<T> GetEntityByIdAsync(int id)
        {
            var response = await _context.Set<T>().FindAsync(id);
            return await Task.FromResult(response);
        }

        public IEnumerable<T> GetEntityByName(string name)
        {
            var response = _context.Set<T>().Where(x => x.Name == name).AsEnumerable();
            return response;
        }

        public async Task<IEnumerable<T>> GetEntityByNameAsync(string name)
        {
            var response = _context.Set<T>().Where(x => x.Name == name).AsEnumerable();
            return await Task.FromResult(response);
        }

        public T UpdateEntity(T entity)
        {
            _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
            return entity;
        }

        public async Task<T> UpdateEntityAsync(T entity)
        {
            _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await _context.SaveChangesAsync();
            return await Task.FromResult(entity);
        }
    }
}
