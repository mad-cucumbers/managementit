﻿using ManagementIt.Core.Abstractions.AppRepository;
using ManagementIt.Core.Domain.ApplicationEntity;
using ManagementIt.DataAccess.DataBase;
using ManagementIt.DataAccess.Repositories.TRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementIt.DataAccess.Repositories.AppEFRepository
{
    public class ApplicationToItEFRepository : GenericEFRepository<ApplicationToIt>, IApplicationTOITRepository
    {
        private readonly AppDbContext _context;

        public ApplicationToItEFRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public List<ApplicationToIt> GetAppByApplicationTypeId(int typeId)
        {
            var response = _context.ApplicationsToIt.Include(x => x.Type).Where(x => x.Type.Id == typeId).ToList();
            return response;
        }

        public async Task<List<ApplicationToIt>> GetAppByApplicationTypeIdAsync(int typeId)
        {
            var response = await _context.ApplicationsToIt.Where(x => x.Type.Id == typeId).ToListAsync();
            return await Task.FromResult(response);
        }

        public List<ApplicationToIt> GetAppByDepartamentId(int depId)
        {
            var response = _context.ApplicationsToIt.ToArray().Where(x => x.Departament.Id == depId).ToList();
            return response;
        }

        public async Task<List<ApplicationToIt>> GetAppByDepartamentIdAsync(int depId)
        {
            var response = await _context.ApplicationsToIt.Where(x => x.Departament.Id == depId).ToListAsync();
            return await Task.FromResult(response);
        }
    }
}
